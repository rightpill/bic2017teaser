 
var selectedLanguage = "ko";
var keyLanguage = "languagea";
var savedLanguage = localStorage.getItem(keyLanguage) || "null";

var lang;
if(savedLanguage == "null"){
    var type= navigator.appName;
    if (type=="Netscape"){
        lang = navigator.language;
    }else{
        lang = navigator.userLanguage;
    }
    selectedLanguage = lang.substr(0,2);
}else{
    selectedLanguage = savedLanguage;
}

localStorage.setItem(keyLanguage, selectedLanguage);

function toKorean(){
    localStorage.setItem(keyLanguage, "ko");
    
    var url = unescape(location.href);
    var pm  = url.split('?');
    window.open(pm[0], '_self');
}
function toEnglish(){
    localStorage.setItem(keyLanguage, "en");
    
    var url = unescape(location.href);
    var pm  = url.split('?');
    window.open(pm[0], '_self');
}

var bi;
var biWidth;
var biHeight;
var bar;
var barWidth;
var barHeight;
var home;
var homeWidth;
var homeHeight;
var submission_menu;
var submission_menuWidth;
var submission_menuHeight;
var korean;
var koreanWidth;
var koreanHeight;
var english;
var englishWidth;
var englishHeight;
var mail;
var mailWidth;
var mailHeight;
var facebook;
var facebookWidth;
var facebookHeight;
var twitter;
var twitterWidth;
var twitterHeight;
var lastYear;
var lastYearWidth;
var lastYearHeight;
var ratio;
function initMenu(){
    bi = document.getElementById("bi");
    biWidth = bi.clientWidth;
    biHeight = bi.clientHeight;
    bar = document.getElementById("bar");
    barWidth = bar.clientWidth;
    barHeight = bar.clientHeight;
    home = document.getElementById("home");
    homeWidth = home.clientWidth;
    homeHeight = home.clientHeight;
    korean= document.getElementById("korean");
    koreanWidth = korean.clientWidth;
    koreanHeight = korean.clientHeight;
    english= document.getElementById("english");
    englishWidth = english.clientWidth;
    englishHeight = english.clientHeight;
    organizer = document.getElementById("organizer");
    organizerWidth = organizer.clientWidth;
    organizerHeight = organizer.clientHeight;
    submission_menu = document.getElementById("submission_menu");
    submission_menuWidth = submission_menu.clientWidth;
    submission_menuHeight = submission_menu.clientHeight;
    mail= document.getElementById("mail");
    mailWidth = mail.clientWidth;
    mailHeight = mail.clientHeight;
    facebook= document.getElementById("facebook");
    facebookWidth = facebook.clientWidth;
    facebookHeight = facebook.clientHeight;
    twitter= document.getElementById("twitter");
    twitterWidth = twitter.clientWidth;
    twitterHeight = twitter.clientHeight;
    lastYear= document.getElementById("last_year");
    lastYearWidth = lastYear.clientWidth;
    lastYearHeight = lastYear.clientHeight;
    
    if (selectedLanguage == "ko") {
        korean.src = "image/top/korean_click.jpg";
        english.src = "image/top/english.jpg";
    }else{
        korean.src = "image/top/korean.jpg";
        english.src = "image/top/english_click.jpg";
    }
}
function rearrangeMenu(){
    width = document.documentElement.clientWidth;
    height = document.documentElement.clientHeight;
    var offsetX = 0;
    if(width > 800){
        offsetX = (width - 800)/2;
        width = 800;
    }
    ratio = width/backgroundWidth;
    
    bi.style.width = biWidth*ratio + "px";
    bi.style.top = window.pageYOffset + 25*ratio + "px";
    bi.style.left = offsetX +  29*ratio + "px";
    bi.style.position = "absolute";
    bar.style.width = barWidth*ratio + "px";
    bar.style.top = window.pageYOffset + 0*ratio + "px";
    bar.style.left = offsetX +  0*ratio + "px";
    bar.style.position = "absolute";
    home.style.width = homeWidth*ratio + "px";
    home.style.top = window.pageYOffset + 166*ratio + "px";
    home.style.left = offsetX + 1073*ratio + "px";
    home.style.position = "absolute";
    submission_menu.style.width = submission_menuWidth*ratio + "px";
    submission_menu.style.top = window.pageYOffset + 166*ratio + "px";
    submission_menu.style.left = offsetX + 1163*ratio + "px";
    submission_menu.style.position = "absolute";
    organizer.style.width = organizerWidth*ratio + "px";
    organizer.style.top = window.pageYOffset + 166*ratio + "px";
    organizer.style.left = offsetX + 1313*ratio + "px";
    organizer.style.position = "absolute";
    korean.style.width = koreanWidth*ratio + "px";
    korean.style.top = window.pageYOffset + 127*ratio + "px";
    korean.style.left = offsetX + 1238*ratio + "px";
    korean.style.position = "absolute";
    english.style.width = englishWidth*ratio + "px";
    english.style.top = window.pageYOffset + 127*ratio + "px";
    english.style.left = offsetX + 1349*ratio + "px";
    english.style.position = "absolute";
    mail.style.width = mailWidth*ratio + "px";
    mail.style.top = window.pageYOffset + 127*ratio + "px";
    mail.style.left = offsetX + 1463*ratio + "px";
    mail.style.position = "absolute";
    facebook.style.width = facebookWidth*ratio + "px";
    facebook.style.top = window.pageYOffset + 127*ratio + "px";
    facebook.style.left = offsetX + 1501*ratio + "px";
    facebook.style.position = "absolute";
    twitter.style.width = twitterWidth*ratio + "px";
    twitter.style.top = window.pageYOffset + 127*ratio + "px";
    twitter.style.left = offsetX + 1539*ratio + "px";
    twitter.style.position = "absolute";
    lastYear.style.width = lastYearWidth*ratio + "px";
    lastYear.style.top = window.pageYOffset + 166* ratio + "px";
    lastYear.style.left = offsetX + 1457*ratio + "px";
    lastYear.style.position = "absolute";
}

window.onscroll = function(event){
    rearrangeMenu();
    
};

